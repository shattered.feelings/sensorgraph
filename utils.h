

#ifndef UTILS_H
#define UTILS_H



class RGB
{
public:
	unsigned char R;
	unsigned char G;
	unsigned char B;

	RGB(){}

	RGB(unsigned char r, unsigned char g, unsigned char b)
	{
		R = r;
		G = g;
		B = b;
	}
void setColor(unsigned char r, unsigned char g, unsigned char b)
	{
		R = r;
		G = g;
		B = b;
	}

	bool Equals(RGB rgb)
	{
		return (R == rgb.R) && (G == rgb.G) && (B == rgb.B);
	}




};



class HSL
{
public:
	int H;  // from 0 to 359
	float S; // from 0.0f to 0.99f
	float L; // from 0.0f to 0.50f

	HSL(){}

	HSL(int h, float s, float l)
	{
		H = h;
		S = s;
		L = l;
	}

  void setColor(int h, float s, float l)
    {
	   H = h;
	   S = s;
	   L = l;
    }

	bool Equals(HSL hsl)
	{
		return (H == hsl.H) && (S == hsl.S) && (L == hsl.L);
	}

	float HueToRGB(float v1, float v2, float vH);
	RGB   toRGB();

};


#endif /* UTILS_H */
