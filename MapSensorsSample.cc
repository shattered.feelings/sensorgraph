/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) June 2019 Ritsumeikan University, Shiga, Japan
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * SensorGraph Application
 *
 *
 * Authors: Alberto Gallegos Ramonet <ramonet@fc.ritsumei.ac.jp>
 */


#include <iostream>
#include <string>
#include <sstream>
#include "SensorGraph.h"

void
Example1()
{
  SensorGraph map (200,200);

  map.LoadNodes("misc/netNodePos");
  map.LoadTraceRoute("misc/netTraceRoutes");

  // Draw multiple branches an their intersections.
  std::vector<std::pair<std::string,int>> branchList;
  branchList.push_back(std::make_pair("10.0.0.7", 8));
  branchList.push_back(std::make_pair("10.0.0.6", 4));

  map.DrawMultiBranch(branchList);
  std::cout<<"Example 1 Results\n";
  std::cout<<"Total Inactive Green Sensor Area: " << map.GetTotalSensorArea(8)<<"\n";
  std::cout<<"Total Inactive Orange Sensor Area: " << map.GetTotalSensorArea(4)<<"\n";
  std::cout<<"Total Active Blue Sensor Area: " << map.GetTotalSensorArea(3)<<"\n";
  map.Window();

}


void
Example2()
{
  SensorGraph map (200,200);

  map.LoadNodes("misc/netNodePos");
  map.LoadTraceRoute("misc/netTraceRoutes");

  // Draw multiple branches an their intersections.
  std::vector<std::pair<std::string,int>> branchList;
  branchList.push_back(std::make_pair("10.0.0.25", 8));
  branchList.push_back(std::make_pair("10.0.0.6", 4));

  map.DrawMultiBranch(branchList);
  std::cout<<"Example 2 Results\n";
  std::cout<<"Total Inactive Green Sensor Area: " << map.GetTotalSensorArea(8)<<"\n";
  std::cout<<"Total Inactive Orange Sensor Area: " << map.GetTotalSensorArea(4)<<"\n";
  std::cout<<"Total Active Blue Sensor Area: " << map.GetTotalSensorArea(3)<<"\n";

  map.Window();

}



void
Example3()
{
  SensorGraph map (200,200);

  map.LoadNodes("misc/netNodePos2");
  map.LoadTraceRoute("misc/netTraceRoutes");

  // Draw multiple branches an their intersections.
  std::vector<std::pair<std::string,int>> branchList;
  branchList.push_back(std::make_pair("10.0.0.7", 8));
  branchList.push_back(std::make_pair("10.0.0.6", 4));

  map.DrawMultiBranch(branchList);
  std::cout<<"Example 3 Results\n";
  std::cout<<"Total Inactive Green Sensor Area: " << map.GetTotalSensorArea(8)<<"\n";
  std::cout<<"Total Inactive Orange Sensor Area: " << map.GetTotalSensorArea(4)<<"\n";
  std::cout<<"Total Active Blue Sensor Area: " << map.GetTotalSensorArea(3)<<"\n";

  map.Window();

}

void
ExampleMisc()
{
  SensorGraph map (200,200);

///  Circular Sensor Area filled with horizontal lines //////////
  Sensor s = Sensor(90,55);
  s.SetRadius(15);
  s.SetAngles(0,20);
  map.DrawSensorArea2(s,8);
////////  Diagonal Line /////////////
  int color = 3;
  map.bresenhamLine (30,40,6,20, color);


//////// Circle filled with diagonal Bresenham lines/////////
  int color2 = 2;
  Sensor s2 = Sensor(100,100);
  s2.SetRadius(19);
  s2.SetAngles(0,360);
  map.DrawSensorArea2(s2,color2);



  map.Window();

}



int main ()
{

  // Only one example at a time.

  //Example1 ();
  //Example2 ();
  Example3 ();
  //ExampleMisc();


}
