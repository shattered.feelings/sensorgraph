/*
 * SensorGraph.h
 *
 *  Created on: Jun 11, 2019
 *      Author: acidcube
 */
#include <vector>
#include <fstream>
#include <map>
#include <SDL2/SDL.h>
#include "utils.h"


#ifndef SENSORGRAPH_H_
#define SENSORGRAPH_H_


struct Positions
{
	int x;
	int y;
};


enum sensorType
{
  CIRCULAR_SENSOR,
  ANGULAR_SENSOR
};


class Sensor {
public:

  Sensor ();
  Sensor (int posX, int posY);
  void SetPositions (int posX, int posY);
  void SetRadius (int rad);
  void SetAngles (int startAngle, int endAngle);
  int GetX ();
  int GetY ();
  int GetStartAngle ();
  int GetEndAngle();
  int GetRadius ();
  sensorType GetType ();

private:

  int m_x,m_y;
  int m_radius;
  sensorType m_typeSensor;
  int m_startAngle;
  int m_endAngle;


};




class SensorGraph {
public:
  SensorGraph(int length, int height);
  virtual ~SensorGraph();

  /**
   * Draw a Circular Sensor Area
   */
  void DrawSensorArea ( Sensor sensor, int Symbol);
  /**
   * Draw a Partial Circular Area according to the
   * radius, start angle, end angle defined in the Sensor
   */
  void DrawSensorArea2 ( Sensor sensor, int Symbol);

  /**
   * Print the contents of the matrix in a text based enviroment.
   * Useful for simple debugging.
   */
  void Print ();
  void Clean ();

  /**
   * Calculate the total area of a sensor in the map
   * (Number of times a symbol appear in the matrix)
   * */
  int GetTotalSensorArea (int symbol);

  /**
   * Loads the the position of the nodes in the network from a file
   * Format (4 elements per line): NodeId Ipv4Address X Y  into a
   * map object in which each element is a Sensor object.
   **/
  void LoadNodes (const char *path);
  /**
   * Loads all the Route in a network from a TraceRoute file and dump
   * them into a vector object (networkRoutes)
   * One complete route per row in the networkRoutes vector.
   **/
  void LoadTraceRoute (const char *path);

  /**
   * Get the ip Addresses forming a branch given a root ip address.
   * */
  std::vector<std::string> GetBranch (std::string ipAddressRoot);

  /**
   * Draw all the sensing areas (circles) from the nodes (ip addresses)
   * forming a branch.
   **/
  void DrawBranch (std::string rootIpAddress, int sensorRadius, int symbol);

  /**
   * Draw multiple branches  and show the coverage of the rest of the nodes
   * in the networks.
   * Branches must
   *
   * @param  An array where elements are a pair of 1 branch ipAddress and its symbol (color)
   **/
  void DrawMultiBranch (std::vector<std::pair<std::string,int>> branches);


  /**
   * Draw the sensor coverage area of all the nodes in the network
   * with symbol (color) specified.
   * */
  void DrawNetCoverage (int symbol);


  Positions GetNodePos (std::string ipAddress);

  void DrawNetTopology (SDL_Renderer* renderer);

  void Window();

  void bresenhamLine(int x1, int y1, int x2, int y2, int symbol);


private:

  SDL_Window* window;
  std::vector< std::vector<int> > matrix;
  std::vector< std::vector<std::string> >networkRoutes;
  std::map <std::string,Sensor> sensorList;
  std::map <std::pair<int,int>,std::vector<int>> m_intersections;
  std::vector <RGB> colorList;
  int m_height;
  int m_length;
  int m_zoomValue;
  bool m_gridFlag;



  void ResizeMap (int x, int y);
  void InitializeMap ();
  void CirclePoints (int centerX, int centerY, int x, int y, int symbol);
  void CirclePoints2 (int centerX, int centerY, int x, int y, int startAngle, int endAngle, int symbol);
  void DrawLine (int x1, int y1, int x2, int y2,int symbol);
  void AddOverlapCoor(std::pair<int,int> pos, int presentSymbol, int newSymbol);


  void Update (SDL_Renderer* renderer);
  void DrawMapFrame (SDL_Renderer* renderer);
  void DrawGrid (SDL_Renderer* renderer);
  void DrawMap (SDL_Renderer* renderer);
  void DrawText(SDL_Surface* screen, std::string text,int size, int x, int y,float fR, float fG, float fB,float bR, float bG, float bB);

};






#endif /* SENSORGRAPH_H_ */
