/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) June 2019 Ritsumeikan University
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * SensorGraph Application
 *
 *
 * Authors: Alberto Gallegos Ramonet <ramonet@fc.ritsumei.ac.jp>
 */

#include "SensorGraph.h"
#include <cmath>
#include <iostream>
#include <sstream>
#include <set>
#include <algorithm>
#include <math.h>
#include "SDL2/SDL2_gfxPrimitives.h"
#include "SDL2/SDL_ttf.h"



#define PI 3.14159265
#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768

SensorGraph::SensorGraph (int length, int height)
: m_zoomValue(3),
  m_gridFlag(false)
{
  ResizeMap(length,height);
}

SensorGraph::~SensorGraph()
{

}



void
SensorGraph::AddOverlapCoor (std::pair<int,int> pos, int presentSymbol, int newSymbol)
{

  std::map <std::pair<int,int>,std::vector<int>>::iterator it;
  it = m_intersections.find(pos);
  if(it == m_intersections.end())
    {
	  if (newSymbol != 3)
	  {
		// new entry to the x,y coordinate: presentSymbol,newSymbol
	    std::vector<int> newSensorList;
	    newSensorList.push_back(presentSymbol);
	    newSensorList.push_back(newSymbol);
	    m_intersections[pos] = newSensorList;
	  }
    }
  else
    {
	  if (newSymbol == 3)
	    {
		  // Overlap with an active sensor, therefore, we erase all inactive sensors overlaps
		  m_intersections.erase(pos);
	    }
	  else
	    {
		  // Search in the symbols registered for that x,y coordinate
		  std::vector <int>::iterator it2;
		  it2 = std::find(it->second.begin(),it->second.end(), newSymbol);
		  if(it2 == it->second.end())
		    {
              // The symbol does not exist in the x,y coord
			  it->second.push_back(newSymbol);
		    }
	    }
    }

}

void
SensorGraph::CirclePoints(int centerX, int centerY, int x, int y, int symbol)
{

  if (x == 0)
    {
	  //matrix [centerY] [centerX + y] = symbol;  //DELETE
	  //matrix [centerY] [centerX - y] = symbol;  //DELETE
	  DrawLine (centerY,centerX-y,centerY,centerX+y,symbol);

	  //if(((centerY+y) >= 0 && centerX >=0) && (matrix[centerY + y] [centerX] == 0))
		//  matrix [centerY + y] [centerX] = symbol;

	//  if(((centerY-y) >=0 && centerX >= 0) && (matrix[centerY - y] [centerX] == 0))
	  //  matrix [centerY - y] [centerX] = symbol;


    }
  else if (x == y)
    {
	 //matrix [centerY + x] [centerX + y] = symbol; //DELETE
	 //matrix [centerY - x] [centerX + y] = symbol;//DELETE
	 //matrix [centerY + x] [centerX - y] = symbol;//DELETE
	 //matrix [centerY - x] [centerX - y] = symbol;//DELETE

	 DrawLine (centerY+x,centerX-y,centerY+x,centerX+y,symbol);
	 DrawLine (centerY-x,centerX-y,centerY-x,centerX+y,symbol);
    }
  else if (x < y)
    {

	 //matrix [centerY + x] [centerX + y] = symbol;
	 //matrix [centerY + x] [centerX - y] = symbol;
	 //matrix [centerY - x] [centerX + y] = symbol;
	 //matrix [centerY - x] [centerX - y] = symbol;
	 //matrix [centerY + y] [centerX + x] = symbol;
	 //matrix [centerY + y] [centerX - x] = symbol;
	 //matrix [centerY - y] [centerX + x] = symbol;
	 //matrix [centerY - y] [centerX - x] = symbol;

	 DrawLine (centerY+x,centerX-y,centerY+x,centerX+y,symbol);
	 DrawLine (centerY-x,centerX-y,centerY-x,centerX+y,symbol);
	 DrawLine (centerY+y,centerX-x,centerY+y,centerX+x,symbol);
	 DrawLine (centerY-y,centerX-x,centerY-y,centerX+x,symbol);
    }
}




void
SensorGraph::bresenhamLine(int x1, int y1, int x2, int y2, int symbol)
{
  bool steep = false;
  if (std::abs(x1-x2)<std::abs(y1-y2))
    {
      std::swap(x1, y1);
	  std::swap(x2, y2);
	  steep = true;
	}

  if (x1>x2)
    {
	  std::swap(x1, x2);
	  std::swap(y1, y2);
	}

  int dx = x2-x1;
  int dy = y2-y1;
  int derror2 = std::abs(dy)*2;
  int error2 = 0;
  int y = y1;
  for (int x=x1; x<=x2; x++)
    {
     /* if (steep)
	    matrix[x] [y] = symbol;
	  else
	    matrix [y][x] = symbol;
      */
      if (steep)
        {
	      if (matrix [x][y] == 0)
  		    {
  			  // Free area, draw any symbol
  			  matrix [x][y] = symbol;
  		    }
  		  else if (matrix[x][y] != symbol)
  		    {
  			  // Previously occupied area

  			  // Register or unregister overlap position (matrix [y][x])
  			  AddOverlapCoor (std::make_pair(x,y), matrix [x][y], symbol);

  			  if (symbol == 3)
  			    {
  			      // symbol 3 (BLUE) reserved
  				  // The area is an area of a functional sensor, overwrite any symbol
  				  matrix [x][y] = symbol;
  				}
  			  else
  			    {
 	  			  matrix [x][y] = 2;  //symbol 2 = color RED , reserved for intersections
  			    }
  		    }
        }
      else
        {
	      if (matrix [y][x] == 0)
  		    {
  			  // Free area, draw any symbol
  			  matrix [y][x] = symbol;
  		    }
  		  else if (matrix[y][x] != symbol)
  		    {
  			  // Previously occupied area

  			  // Register or unregister overlap position (matrix [y][x])
  			  AddOverlapCoor (std::make_pair(x,y), matrix [y][x], symbol);

  			  if (symbol == 3)
  			    {
  			      // symbol 3 (BLUE) reserved
  				  // The area is an area of a functional sensor, overwrite any symbol
  				  matrix [y][x] = symbol;
  				}
  			  else
  			    {
 	  			  matrix [y][x] = 2;  //symbol 2 = color RED , reserved for intersections
  			    }
  		    }
        }



		error2 += derror2;
		if (error2 > dx)
		  {
			y += (y2>y1?1:-1);
			error2 -= dx*2;
		  }
	}
}






void
SensorGraph::DrawLine (int x1, int y1, int x2, int y2,int symbol)
{

  // Symbol representing the color of overlap zones in the matrix
 // int interSymbol;

  for(int i=y1; i<=y2; i++)
    {
	  if ((x1 >= 0 && x1 <= (int ) matrix.size()-1) && (i >= 0 && i <=(int) matrix[0].size()-1))
	  	    {
		      if (matrix [x1][i] == 0)
	  		    {
	  			  // Free area, draw any symbol
	  			  matrix [x1][i] = symbol;
	  		    }
	  		  else if (matrix[x1][i] != symbol)
	  		    {
	  			  // Previously occupied area

	  			  // Register or unregister overlap position (matrix [y][x])
	  			  AddOverlapCoor (std::make_pair(i,x1), matrix [x1][i], symbol);

	  			  if (symbol == 3)
	  			    {
	  			      // symbol 3 (BLUE) reserved
	  				  // The area is an area of a functional sensor, overwrite any symbol
	  				  matrix [x1][i] = symbol;
	  				}
	  			  else
	  			    {
	 	  			  matrix [x1][i] = 2;  //symbol 2 = color RED , reserved for intersections
	  			    }
	  		    }
	  	    }
    }
}





void
SensorGraph::CirclePoints2(int centerX, int centerY, int x, int y, int startAngle, int endAngle, int symbol)
{

  // obtain the angle of the given x,y circumference points
  int angle = (int)	(atan2(y,x) * 180/ PI);

  if (x==0)
    {
	  // between first and 4th quadrant  (0 degrees)
	  if (((90 - angle) >= startAngle) && ((90 - angle) <= endAngle))
	    bresenhamLine (centerX, centerY, centerX + y , centerY , symbol);
	    //matrix [centerY] [centerX + y] = symbol;

	  // between 2nd and 3rd quadrant(180 degrees)
	  if (angle + 90 >= startAngle && angle + 90 <= endAngle)
		bresenhamLine (centerX, centerY, centerX -y , centerY, symbol);
	    //matrix [centerY] [centerX - y] = symbol;


	  // between 3rd and 4th quadrants (270 degrees)
	  if (360 -angle >= startAngle && 360 - angle <= endAngle)
		bresenhamLine (centerX, centerY, centerX, centerY + y, symbol);
	    //matrix [centerY + y] [centerX] = symbol;

	  // between 1st and 2nd quadrants (90 degrees)
	  if ((angle >= startAngle) && (angle <= endAngle))
		bresenhamLine (centerX, centerY, centerX, centerY - y, symbol);
	    //matrix [centerY - y] [centerX] = symbol;
    }
  else if (x == y)
    {
	  // 4th quadrant line in 270 degrees
	  if (angle + 180 >= startAngle && angle + 180 <= endAngle)
	    bresenhamLine (centerX, centerY, centerX + y, centerY + x, symbol);
	    //matrix [centerY + x] [centerX + y] = symbol;

	  // 1st quadrant, line in 45 degrees
	  if ((angle >= startAngle) && (angle <= endAngle))
	    bresenhamLine (centerX, centerY, centerX +y, centerY - x, symbol);
	    //matrix [centerY - x] [centerX + y] = symbol;

	  // 3rd quadrant, line  225 degrees
	  if (270 - angle >= startAngle && 270 - angle <= endAngle)
	    bresenhamLine (centerX, centerY, centerX -y, centerY + x, symbol);
	    //matrix [centerY + x] [centerX - y] = symbol;

	  // 2nd quadrant, line 135 degrees
	  if (180 - angle >= startAngle && 180 - angle <= endAngle)
	    bresenhamLine (centerX, centerY, centerX - y, centerY - x,symbol);
	    // matrix [centerY - x] [centerX - y] = symbol;

    }
  else if (x < y)
    {

     // Quadrant 1: 0 degrees to 45
	 if (((90 - angle) >= startAngle) && ((90 - angle) <= endAngle))
		bresenhamLine (centerX, centerY, centerX + y, centerY - x, symbol);
	   // matrix [centerY - x] [centerX + y] = symbol;

	 // Quadrant 1: 45 degrees to 90
	 if ((angle >= startAngle) && (angle <= endAngle))
		 bresenhamLine (centerX, centerY, centerX + x, centerY - y, symbol);
		 //matrix [centerY - y] [centerX + x] = symbol;

	 // Quadrant 2: 90 degrees to 135
	 if (180 - angle >= startAngle && 180 - angle <= endAngle)
		 bresenhamLine (centerX, centerY, centerX - x, centerY - y, symbol);
		 //matrix [centerY - y] [centerX - x] = symbol;

	 // Quadrant 2: 135 degrees to 180
	 if (angle + 90 >= startAngle && angle + 90 <= endAngle)
		 bresenhamLine (centerX,centerY, centerX-y, centerY -x, symbol);
		 //matrix [centerY - x] [centerX - y] = symbol;

	 // Quadrant 3: 180 degrees to 225
	 if (270 - angle >= startAngle && 270 - angle <= endAngle)
		 bresenhamLine (centerX, centerY, centerX -y, centerY + x, symbol);
		 //matrix [centerY + x] [centerX - y] = symbol;

	 // Quadrant 3: 225 degrees to 270
	 if (angle + 180 >= startAngle && angle + 180 <= endAngle)
		 bresenhamLine (centerX, centerY, centerX - x, centerY + y, symbol);
		 //matrix [centerY + y] [centerX - x] = symbol;

	 // Quadrant 4: 270 degrees to 315
	 if (360 -angle >= startAngle && 360 - angle <= endAngle)
		 bresenhamLine (centerX, centerY, centerX + x, centerY + y, symbol);
		 //matrix [centerY + y] [centerX + x] = symbol;

	 // Quadrant 4: 315 degrees to 360
	 if (angle + 270 >= startAngle && angle +270 <= endAngle)
		 bresenhamLine (centerX, centerY, centerX + y, centerY + x, symbol);
	    // matrix [centerY + x] [centerX + y] = symbol;

    }
}


void
SensorGraph::DrawSensorArea2(Sensor sensor, int symbol)
{
  int x = 0;
  int y = sensor.GetRadius();
  int p = (5 - sensor.GetRadius()*4)/4;

  int startAngle = sensor.GetStartAngle();
  int endAngle  =  sensor.GetEndAngle();



  CirclePoints2(sensor.GetX(), sensor.GetY(), x, y, startAngle,endAngle, symbol);
  while (x <= y)
    {
      x++;
      if (p < 0)
        {
          p += 2*x+1;
        }
      else
        {
          y--;
          p += 2*(x-y)+1;
        }
      CirclePoints2(sensor.GetX(), sensor.GetY(), x, y, startAngle, endAngle, symbol);
    }
  /// Draw the center of the circle in black (the sensor position)
  matrix[sensor.GetY()][sensor.GetX()] = 1;
}




void
SensorGraph::DrawSensorArea(Sensor sensor, int symbol)
{
  int x = 0;
  int y = sensor.GetRadius();
  int p = (5 - sensor.GetRadius()*4)/4;


  CirclePoints(sensor.GetX(), sensor.GetY(), x, y, symbol);
  while (x < y)
    {
      x++;
      if (p < 0)
        {
          p += 2*x+1;
        }
      else
        {
          y--;
          p += 2*(x-y)+1;
        }
      CirclePoints(sensor.GetX(), sensor.GetY(), x, y, symbol);
    }
  /// Draw the center of the circle in black (the sensor position)
  matrix[sensor.GetY()][sensor.GetX()] = 1;
}



void SensorGraph::LoadNodes (const char *path)
{
  std::string line;
  std::string word;
  std::string ipAddress;
  int x,y;
  int nodeId;
  int radius;
  int startAngle;
  int endAngle;
  int countWords;


  std::ifstream file (path);
  while (std::getline(file, line))
    {
	  std::istringstream iss(line);
	  if (line[0] !='#') // Ignore lines with comments (lines that start with #)
	   {
		 countWords = 0;
		 radius = -1;
		 startAngle = -1;
		 endAngle = -1;

		 while (iss>>word)
		   {
			 switch (countWords)
			   {
			     case 0:
			    	 nodeId = stoi(word);
			    	 break;
			     case 1:
			    	 ipAddress = word;
			    	 break;
			     case 2:
			    	 x = stoi(word);
			    	 break;
			     case 3:
			    	 y = stoi(word);
			    	 break;
			     case 4:
			    	 radius = stoi(word);
			    	 break;
			     case 5:
			    	 startAngle = stoi(word);
			    	 break;
			     case 6:
			    	 endAngle = stoi(word);
			    	 break;
			     default:
			    	 break;
			   }
			 countWords++;
		   }
/*
		 if (!(iss >> nodeId >> ipAddress >> x >> y))
		   {
			 std::cout<<"ERROR breaking netNOdeps\n";   // error
			 break;
		   }
*/
		 if (countWords == 4)
		   {
			 // Default number of parameters
			 // Create a circular sensor area of radius = 15
			 sensorList[ipAddress] = Sensor (x,y);
		   }
		 else if (countWords == 5 )
		   {
			 // Create a circular sensor area of a given radius
			 Sensor s = Sensor();
			 s.SetPositions(x,y);
			 s.SetRadius(radius);
			 sensorList[ipAddress] = s;
		   }
		 else if (countWords == 7)
		   {
			 // Create a partial circular sensor area with a given
			 // radius, startAngle and endAngle
			 Sensor s = Sensor();
			 s.SetPositions(x,y);
			 s.SetRadius(radius);
			 s.SetAngles(startAngle,endAngle);
			 sensorList[ipAddress] = s;
		   }
		 else
			 std::cout<<"Error wrong number of parameters in line: "<<line<<"\n";



		 std::cout<<"nodeId "<<nodeId
		          <<" IpAddress "<<ipAddress
				  <<" X "<< x
				  <<" Y "<< y
				  <<" radius "<< radius
				  <<" startAngle "<< startAngle
				  <<" endAngle "<< endAngle
				  <<"\n";
	   }
	}
  std::cout<<"Nodes successfully Loaded.\n";
}



void SensorGraph::LoadTraceRoute (const char *path)
{
  std::string line;
  std::string ipAddress;
  int routeNumber;

  std::vector<std::string> oneRoute;

  //std::cout<<"Trace Routes loaded:\n";
  std::ifstream file (path);
  while (std::getline(file, line))
	{
	  std::istringstream iss(line);

	  if (iss >> routeNumber)
		{
		  if(!(iss >> ipAddress)){ break;}

		   if(ipAddress != "*")
		     {
			  // std::cout<<ipAddress<<" ";
			   oneRoute.push_back(ipAddress);
		     }
		}
	  else
	    {
		  if(oneRoute.size() > 0)
		    {
			  networkRoutes.push_back(oneRoute);
			  //std::cout<<"\n";
		    }


          //swap vector for an empty vector to deallocate.
          std::vector <std::string>().swap(oneRoute);
	    }
	}
  std::cout<<"Routes successfully Loaded.\n";
}

/*
std::vector<std::string>
SensorGraph::GetAllRoutes(std::string ipAddressRoot)
{
  std::vector<std::string> branch;
  bool nodeFound = false;
  std::string ipAddress;

  if(networkRoutes.size() > 0)
    {
	  std::vector< std::vector<std::string> >::iterator it;
      for(it = networkRoutes.begin(); it != networkRoutes.end(); ++it)
        {
    	  std::vector<std::string>::iterator it2;
    	  for(it2 = it->begin(); it2 != it->end(); ++it2)
    	    {
    		  ipAddress = *it2; //1 node (ip address) from a route

              if(nodeFound)
                {
            	  if (std::find(branch.begin(), branch.end(), ipAddress) == branch.end())
            		  branch.push_back(ipAddress);
                }
              else
                {
            	  if (ipAddress == ipAddressRoot)
            	    {
            		  nodeFound = true;
            		  if (std::find(branch.begin(), branch.end(), ipAddress) == branch.end())
            			  branch.push_back(ipAddress);
            	    }
                }
    	    }
    	  nodeFound = false;
        }

    }
  else
    {
	  std::cout<<"No Network Found, please load the network routes first.\n";
    }

  return branch;
}
*/


std::vector<std::string>
SensorGraph::GetBranch (std::string ipAddressRoot)
{
  std::vector<std::string> branch;
  std::string ipAddress;

  if(!networkRoutes.empty())
    {
	  std::vector< std::vector<std::string> >::iterator it;
      for(it = networkRoutes.begin(); it != networkRoutes.end(); ++it)
        {
    	 std::vector <std::string> row;
    	 row = *it;
         /// Serch element in 1 route
    	 if(std::find(row.begin(),row.end(),ipAddressRoot) != row.end())
    	   {
    		 // if the element is not added to the branch list, add it.
    		 if (std::find(branch.begin(), branch.end(), ipAddress) == branch.end())
    		   branch.push_back(row[row.size()-1]);
    	   }
        }
    }
  else
    {
	  std::cout<<"No Network Found, please load the network routes first.\n";
    }

  return branch;
}



void
SensorGraph::DrawBranch (std::string rootIpAddress,int sensorRadius, int symbol)
{
  std::vector <std::string> branch;
  std::string ipAddress;
  Sensor sensor;

  branch = GetBranch (rootIpAddress);

  if (branch.empty())
    {
	  std::cout<<" Cannot draw branch, no route traces loaded\n";
	  return;
    }
  if (sensorList.empty())
    {
	  std::cout<<" Cannot draw branch, nodes positions not loaded\n";
	  return;
    }

  std::vector<std::string>::iterator it;
  for(it = branch.begin(); it != branch.end(); ++it)
    {
	  sensor = sensorList.find(*it)->second;
	  std::cout<<"Branch node x y: "<<sensor.GetX()<<" "<<sensor.GetY()<<"\n";
	  DrawSensorArea(sensor,symbol);  //pos.y,pos.x,sensorRadius,symbol
	}

}

void
SensorGraph::DrawMultiBranch (std::vector<std::pair<std::string,int>> branches)
{
  std::vector <std::string> branch;
  std::string ipAddress;
  Sensor sensor;


  if (branches.size() < 1)
   {
	  std::cout<<" The branches array is empty!.\n";
	  return;
   }

  // A list with all the functional sensor in the network
  // this list changes after all the branches are removed.
  std::map <std::string,Sensor> functionalSensors = sensorList;
  std::set <std::string> nonFuncSensors;

  std::vector<std::pair<std::string,int>> ::iterator mainIt;
  for (mainIt = branches.begin(); mainIt != branches.end(); ++mainIt)
    {

	  branch = GetBranch (mainIt->first);
	  if (branch.empty())
		{
		  std::cout<<"Cannot draw branch, no route traces file loaded.\n";
		  return;
		}

	  if (functionalSensors.empty())
		{
		  std::cout<<" Cannot draw branch, nodes positions not loaded\n";
		  return;
		}

	  std::vector<std::string>::iterator it;
	  for(it = branch.begin(); it != branch.end(); ++it)
		{
		  sensor = functionalSensors.find(*it)->second;

		  if (sensor.GetType() == sensorType::CIRCULAR_SENSOR)
		    DrawSensorArea(sensor,mainIt->second);
		  else if (sensor.GetType() == sensorType::ANGULAR_SENSOR)
			DrawSensorArea2(sensor,mainIt->second);

		  nonFuncSensors.insert(*it);
		}

    }

    // remove from the sensor list all the sensor that are part of a branch
    std::set<std::string>::iterator rmvIt;
    for(rmvIt = nonFuncSensors.begin(); rmvIt != nonFuncSensors.end(); ++rmvIt)
      {
        functionalSensors.erase(*rmvIt);
      }

    // DrawSensorArea of functionalSensors
    std::map <std::string,Sensor>::iterator funIt;
    for (funIt = functionalSensors.begin(); funIt != functionalSensors.end(); ++funIt)
      {
  	    sensor = funIt->second;
  	    // draw functional Sensors with reserved symbol 3 (Active Sensors in Blue)
		if (sensor.GetType() == sensorType::CIRCULAR_SENSOR)
		  DrawSensorArea(sensor,3);
		else if (sensor.GetType() == sensorType::ANGULAR_SENSOR)
	      DrawSensorArea2(sensor,3);
      }


}


void
SensorGraph::DrawNetCoverage (int symbol)
{
  std::vector <std::string> branch;
  Sensor sensor;

  if (sensorList.empty())
    {
	  std::cout<<" Cannot draw branch, nodes positions not loaded\n";
	  return;
    }

  std::map <std::string,Sensor>::iterator it;
  for (it = sensorList.begin(); it != sensorList.end(); ++it)
    {
	  sensor = it->second;
	  DrawSensorArea(sensor,symbol);
    }
}




void
SensorGraph::Window()
{
  window = NULL;
  SDL_Surface* screenSurface = NULL;
  SDL_Renderer* renderer = NULL;

  if (SDL_Init(SDL_INIT_VIDEO) < 0)
	fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());

  window = SDL_CreateWindow(
				"SensorGraph V0.2Beta",
				SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
				SCREEN_WIDTH, SCREEN_HEIGHT,
				SDL_WINDOW_SHOWN
				);
  if (window == NULL)
	fprintf(stderr, "could not create window: %s\n", SDL_GetError());

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
  if (renderer == NULL)
	  fprintf(stderr, "could not create renderer: %s\n", SDL_GetError());

  SDL_RenderClear(renderer);


  TTF_Init();


  screenSurface = SDL_GetWindowSurface(window);
  SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));
  SDL_RenderPresent(renderer);


  // Create a list of 20 colors
  // First 4 colors are RESERVED for special purposes:
  // 0. WHITE - Empty spaces in the map
  // 1. BLACK - The node location
  // 2. RED   - The overlap areas (intersection between 2 or more sensor coverage areas)
  // 3. BLUE  - Functional Sensor Coverage areas

  int hue = 0;
  int hue2 = 210;
  float sat = 0.99f;
  colorList.push_back(HSL(0.0f,0.0f,0.99f).toRGB()); //First element WHITE
  colorList.push_back(HSL(0.0f,0.99f,0.0f).toRGB()); //Second element BLACK
  for(int i=1; i<=20; i++)
    {
	  if(i % 2 != 0)
	    {
		  colorList.push_back(HSL(hue,sat,0.50f).toRGB());
		  hue = hue + 30;
		}
	  else
	    {
		  colorList.push_back(HSL(hue2,sat,0.50f).toRGB());
		  hue2 = hue2 - 30;
		}
    }

  SDL_Event e;
  bool quit = false;

  while (!quit)
    {
	  while (SDL_PollEvent(&e) != 0)
	    {
		  switch (e.type)
		    {
		      case SDL_QUIT:
			    quit = true;
			    break;

		      case SDL_KEYDOWN:
		    	switch(e.key.keysym.sym)
		    	  {
		    	    case SDLK_q:
		    	    	if(m_zoomValue != 12)
		    	          m_zoomValue++;
		    	    	break;
		    	    case SDLK_w:
		    	    	if(m_zoomValue != 1)
		    	    	  m_zoomValue--;
		    	    	break;
		    	    case SDLK_g:
		    	    	if(m_gridFlag)
		    	    		m_gridFlag = false;
		    	    	else
		    	    		m_gridFlag = true;
		    	    	break;
                     ///Write al the switch down events here.

		    	  }//End of KeyDown Switch
		    }
	    }//end of pool event while

	  screenSurface = SDL_GetWindowSurface(window);
	  SDL_FillRect( screenSurface,
			        NULL,
					SDL_MapRGB( screenSurface->format, 0xFF, 0xFF, 0xFF ) );  //redraw background


	  Update (renderer);


	  SDL_RenderPresent(renderer);
	  SDL_Delay (40);


    }//end of while main loop


  TTF_Quit ();
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();


}



void
SensorGraph::InitializeMap()
{

  for(unsigned int i=0; i< matrix.size(); i++)
    {
      for(unsigned int j=0; j< matrix[i].size(); j++)
        {
    	  matrix[i][j] = 0;
    	  //std::cout<<j<<" "<<i<<" |";
        }
     // std::cout<<"\n";
    }
}




void
SensorGraph::ResizeMap(int length, int height)
{
  //Grow Map height x number of spaces
  m_height = height; // rows
  m_length = length; // columns


  matrix.resize(m_height);
  for(int i = 0 ; i < m_height ; ++i)
    {
	  //Grow Map length x number of spaces
	  matrix[i].resize(m_length);
	}

   InitializeMap  ();
}

void
SensorGraph::Print ()
{
  for(unsigned int i=0; i < matrix.size(); i++)
    {
      for(unsigned int j=0; j < matrix[i].size(); j++)
        {
    	  std::cout<<matrix[i][j];
        }
      std::cout<<"\n";
    }
}

void
SensorGraph::Clean ()
{
  for(unsigned int i=0; i < matrix.size(); i++)
    {
      for(unsigned int j=0; j < matrix[i].size(); j++)
        {
    	  matrix[i][j] = 0;
        }
    }
}



int
SensorGraph::GetTotalSensorArea (int symbol)
{
  int totalArea = 0;

  for(unsigned int i=0; i < matrix.size(); i++)
    {
      for(unsigned int j=0; j < matrix[i].size(); j++)
        {
    	  if (matrix[i][j] == 2)
    	    {
    		  std::vector<int> symbolList;
    		  symbolList = m_intersections.at(std::make_pair(j,i));

    		  std::vector <int>::iterator it;
    		  it = std::find(symbolList.begin(),symbolList.end(), symbol);
    		  if(it != symbolList.end())
    		    {
    			  // overlap for that symbol found
    			  totalArea++;
    		    }
    	    }
    	  else if (matrix[i][j] == symbol)
    		  totalArea++;

        }
    }
  return totalArea;
}


void
SensorGraph::Update (SDL_Renderer* renderer)
{

  DrawMap (renderer);
  DrawMapFrame (renderer);

  if (m_gridFlag)
    DrawGrid (renderer);

  DrawNetTopology(renderer);
}

void
SensorGraph::DrawMapFrame (SDL_Renderer* renderer)
{


  RGB color = HSL(0.0f,0.99f,0.0f).toRGB();
  SDL_SetRenderDrawColor(renderer,color.R,color.G, color.B, 255);


  if(matrix.size() == 0)
	  return;


  int x1, y1, x2, y2;
  int elementsX = matrix.size();
  int elementsY = matrix.at(0).size();


  x1 = (SCREEN_WIDTH - (elementsX * m_zoomValue))/2;
  y1 = (SCREEN_HEIGHT -(elementsY * m_zoomValue))/2;
  x2 = x1 + (elementsX * m_zoomValue);
  y2 = y1;

  SDL_RenderDrawLine(renderer, x1, y2, x2, y2);

  x1 = (SCREEN_WIDTH - (elementsX * m_zoomValue))/2;
  y1 = (SCREEN_HEIGHT -(elementsY * m_zoomValue))/2 + (elementsY * m_zoomValue);
  x2 = x1 + (elementsX * m_zoomValue);
  y2 = y1;

  SDL_RenderDrawLine(renderer, x1, y2, x2, y2);

  x1 = (SCREEN_WIDTH - (elementsX * m_zoomValue))/2;
  y1 = (SCREEN_HEIGHT -(elementsY * m_zoomValue))/2;
  x2 = x1;
  y2 = y1 + (elementsY * m_zoomValue);

  SDL_RenderDrawLine(renderer, x1, y1, x2, y2);


  x1 = (SCREEN_WIDTH - (elementsX * m_zoomValue))/2 + (elementsX * m_zoomValue);
  y1 = (SCREEN_HEIGHT -(elementsY * m_zoomValue))/2;
  x2 = x1;
  y2 = y1 + (elementsY * m_zoomValue);

  SDL_RenderDrawLine(renderer, x1, y1, x2, y2);

}


void
SensorGraph::DrawGrid (SDL_Renderer* renderer)
{
  RGB color = HSL(2.00f,0.09f,0.77f).toRGB();
  SDL_SetRenderDrawColor(renderer,color.R,color.G, color.B, 255);


  if(matrix.size() == 0)
	  return;

  int x1, y1, x2, y2;
  int lastX,lastY;
  int elementsX = matrix.size();
  int elementsY = matrix.at(0).size();

  // Draw the grid horizontal lines
  x1 = (SCREEN_WIDTH - (elementsX * m_zoomValue))/2+1 ;
  y1 = (SCREEN_HEIGHT -(elementsY * m_zoomValue))/2+m_zoomValue;
  x2 = x1 + (elementsX * m_zoomValue)-2;
  y2 = y1;

  lastY = ((SCREEN_HEIGHT -(elementsY * m_zoomValue))/2) + (elementsY * m_zoomValue);

  while (y1 < lastY)
    {
	  SDL_RenderDrawLine(renderer, x1, y2, x2, y2);
	  y1 = y1 + m_zoomValue;
	  y2 = y1;
    }


  // Draw the grid Vertical lines
  x1 = (SCREEN_WIDTH - (elementsX * m_zoomValue))/2 + m_zoomValue;
  y1 = (SCREEN_HEIGHT -(elementsY * m_zoomValue))/2+1;
  x2 = x1;
  y2 = y1 + (elementsY * m_zoomValue)-2;

  lastX = ((SCREEN_WIDTH - (elementsX * m_zoomValue))/2) + (elementsX * m_zoomValue);

  while (x1 < lastX)
    {
	  SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
	  x1 = x1 + m_zoomValue;
	  x2 = x1;
    }
}


void
SensorGraph::DrawMap (SDL_Renderer* renderer)
{

  int x,y, initX;
  RGB  rgbColor;
  int elementsX = matrix.size();
  int elementsY = matrix.at(0).size();

  initX = (SCREEN_WIDTH - (elementsX * m_zoomValue))/2;
  x = initX;
  y = (SCREEN_HEIGHT -(elementsY * m_zoomValue))/2;

  for(unsigned int i=0; i < matrix.size(); i++)
	{
	  for(unsigned int j=0; j < matrix[i].size(); j++)
		{
		    rgbColor = colorList.at(matrix[i][j]);
		    SDL_SetRenderDrawColor(renderer,rgbColor.R,rgbColor.G, rgbColor.B, 255 );
			SDL_Rect fillRect = { x, y, m_zoomValue, m_zoomValue };
			SDL_RenderFillRect( renderer, &fillRect );
			x = x + m_zoomValue;
		}
	  x = initX;
	  y = y + m_zoomValue;
	 // std::cout<<"\n";
	}

}

void
SensorGraph::DrawNetTopology (SDL_Renderer* renderer)
{
  int initX, initY;
  RGB  rgbColor;
  int elementsX = matrix.size();
  int elementsY = matrix.at(0).size();

  // The initial positions for the frame inside the program window.
  initX = (SCREEN_WIDTH - (elementsX * m_zoomValue))/2;
  initY = (SCREEN_HEIGHT -(elementsY * m_zoomValue))/2;

  rgbColor = colorList.at(1); // color BLACK
  SDL_SetRenderDrawColor(renderer,rgbColor.R,rgbColor.G, rgbColor.B, 255 );
  // x, y, width, height

  int x1,y1,x2,y2;
  SDL_Surface* screenSurface = SDL_GetWindowSurface(window);

  // Draw Node points int the map
  std::map<std::string,Sensor>::iterator it;
  for (it=sensorList.begin(); it!=sensorList.end(); ++it)
    {
	  Sensor sensor;
	  sensor = it->second;

	  x1 = initX +(sensor.GetX() * m_zoomValue);
	  y1 = initY +(sensor.GetY() * m_zoomValue);


	  x2 = x1 + (m_zoomValue +2);
	  y2 = y1 + (m_zoomValue+2);

	  DrawText(screenSurface,it->first,11,x2,y2,
	 			                 0,0.99,0,
	 							 0,0,0.99);


	  SDL_Rect fillRect = {x1, y1, m_zoomValue, m_zoomValue };
	  SDL_RenderFillRect ( renderer, &fillRect );

    }

  // Draw the links between the nodes (routes)
  std::vector <std::vector<std::string>>::iterator it2;
  for (it2=networkRoutes.begin(); it2!=networkRoutes.end(); ++it2)
    {
	  Positions pos1,pos2;
	  std::vector<std::string> route;
	  route = *it2;
	  if (route.size () == 1)
	    {
		  pos1 = GetNodePos ("10.0.0.1");
		  pos2 = GetNodePos (route[0]);
		  x1 = initX +(pos1.x * m_zoomValue);
		  y1 = initY +(pos1.y * m_zoomValue);
		  x2 = initX +(pos2.x * m_zoomValue);
		  y2 = initY +(pos2.y * m_zoomValue);

		  SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
	    }
	  else
	    {
		  std::vector<std::string>::iterator it3;
		  std::string prevNode;
		  int index = 0;
		  for (it3=route.begin(); it3!=route.end(); ++it3)
		    {
			  if (index != 0)
			    {
				  pos2 = GetNodePos (*it3);
				  x1 = initX +(pos1.x * m_zoomValue);
				  y1 = initY +(pos1.y * m_zoomValue);
				  x2 = initX +(pos2.x * m_zoomValue);
				  y2 = initY +(pos2.y * m_zoomValue);

				  SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
				  pos1 = pos2;
			    }
			  else
			    {
				  pos1 = GetNodePos (*it3);
			    }
              index ++;
		    }
	    }
    }

}



void
SensorGraph::DrawText
(SDL_Surface* screen, std::string text,
int size, int x, int y,
float fR, float fG, float fB,
float bR, float bG, float bB)
{

TTF_Font* font = TTF_OpenFont("fonts/OpenSans-Regular.ttf", size);

RGB fColor = HSL(fR,fG,fB).toRGB();
SDL_Color foregroundColor = { fColor.R, fColor.G, fColor.B };
RGB bColor = HSL(bR,bG,bB).toRGB();
SDL_Color backgroundColor = { bColor.R, bColor.G, bColor.B };

SDL_Surface* textSurface
= TTF_RenderText_Shaded
(font, text.c_str(), foregroundColor, backgroundColor);

SDL_Rect textLocation = { x, y, 0, 0 };

SDL_BlitSurface(textSurface, NULL, screen, &textLocation);

SDL_FreeSurface(textSurface);

TTF_CloseFont(font);

}


Positions
SensorGraph::GetNodePos (std::string ipAddress)
{
  Positions pos;
  Sensor sensor = sensorList.find(ipAddress)->second;
  pos.x = sensor.GetX();
  pos.y = sensor.GetY();
  return pos;
}











Sensor::Sensor ()
: m_x(0),
  m_y(0),
  m_radius(15),
  m_typeSensor(CIRCULAR_SENSOR),
  m_startAngle(-1),
  m_endAngle(-1)
{


}

Sensor::Sensor(int posX, int posY)
: m_x(0),
  m_y(0),
  m_radius(15),
  m_typeSensor(CIRCULAR_SENSOR),
  m_startAngle(-1),
  m_endAngle(-1)
{
  m_x = posX;
  m_y = posY;

}

sensorType
Sensor::GetType ()
{
 return m_typeSensor;
}

void
Sensor::SetPositions (int posX, int posY)
{
  m_x = posX;
  m_y = posY;
}

void
Sensor::SetRadius (int rad)
{
  m_radius = rad;
}

void
Sensor::SetAngles (int startAngle, int endAngle)
{
  m_startAngle = startAngle;
  m_endAngle = endAngle;
  m_typeSensor = ANGULAR_SENSOR;

}



int
Sensor::GetX ()
{
  return m_x;
}

int
Sensor::GetY ()
{
  return m_y;
}

int
Sensor::GetStartAngle ()
{
  return m_startAngle;
}

int
Sensor::GetEndAngle ()
{
  return m_endAngle;
}


int
Sensor::GetRadius ()
{
  return m_radius;
}


